package main

import (
	"context"
	"time"

	"gopkg.in/cheggaaa/pb.v1"
)

func executeJobs(server string, url string, workers int, requestJobs int) []Result {
	savedResults := []Result{}
	bar := pb.StartNew(requestJobs)

	jobs := make(chan int, requestJobs)
	results := make(chan Result, requestJobs)

	for w := 0; w < workers; w++ {
		go workerGet(server, url, jobs, results)
	}

	for r := 0; r < requestJobs; r++ {
		jobs <- r
	}
	close(jobs)

	for a := 0; a < requestJobs; a++ {
		r := <-results
		savedResults = append(savedResults, r)
		bar.Increment()
	}

	return savedResults
}

func executeTimedJobs(server string, url string, durationMinutes int, requestsPerMinute int) []Result {
	savedResults := []Result{}

	totalRequests := durationMinutes * requestsPerMinute

	bar := pb.StartNew(totalRequests)

	rate := time.Minute / time.Duration(requestsPerMinute)
	throttle := time.Tick(rate)

	results := make(chan Result)

	ctx, _ := context.WithTimeout(context.Background(), time.Duration(durationMinutes)*time.Minute)

	for r := 0; r < totalRequests; r++ {
		<-throttle
		go workerTimedGet(ctx, server, url, results)
		bar.Increment()
	}

	for a := 0; a < totalRequests; a++ {
		r := <-results
		savedResults = append(savedResults, r)
	}

	return savedResults
}
