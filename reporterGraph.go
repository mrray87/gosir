package main

import (
	"os"
	"strings"
	"gosir/config"

	"github.com/wcharczuk/go-chart"
)

func buildChartData(results []Result, nodes int) ([]float64, []float64) {
	var yNodes []float64

	limit := len(results) / nodes
	counter := 1

	for _, r := range results {
		if counter == limit {
			yNodes = append(yNodes, float64(r.Time))
			counter = 1
		}
		counter++
	}

	var xNodes []float64
	for n := 0; n < nodes; n++ {
		xNodes = append(xNodes, float64(n+1))
	}

	return xNodes, yNodes
}

func drawChart(results []Result, target string, scenario string) {

	xNodes, yNodes := buildChartData(results, config.Nodes)

	graph := chart.Chart{
		Title:  "Scenario: " + scenario + "\n" + "Target: " + target,
		YAxis:  chart.YAxis{Style: chart.StyleShow()},
		Height: 400,
		Width:  1000,
		Series: []chart.Series{
			chart.ContinuousSeries{
				Style: chart.Style{
					Show:        true,
					StrokeColor: chart.GetDefaultColor(0).WithAlpha(64),
					FillColor:   chart.GetDefaultColor(0).WithAlpha(64),
				},
				XValues: xNodes,
				YValues: yNodes,
			},
		},
	}

	file, err := os.Create("reports/" +
		strings.Join(strings.Split(scenario, " "), "-") + "-" +
		strings.Join(strings.Split(target, "/"), "-") + ".png")
	if err != nil {
		panic(err)
	}

	graph.Render(chart.PNG, file)
	defer file.Close()
}
