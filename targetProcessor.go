package main

import (
	"fmt"
	"gosir/config"
	"time"
)

func multipleTargets(s config.Scenario) []string {
	var targets []string
	for _, t := range s.Targets {
		targets = append(targets, t.URL)
	}
	return targets
}

func processTargets(cfg string, env string) *reportData {

	config := config.GetConfig(cfg)

	start := time.Now()

	if len(env) == 0 {
		env = config.URL
	}

	fmt.Printf("*** TESTING STARTED\nServer URL: %v\n", env)
	fmt.Printf("\n")

	reportData := &reportData{}
	reportData.generateReportHeaderData(env, config, config.Thresholds)

	for _, s := range config.Scenarios {

		fmt.Printf("*** Scenario: %v", s.Name)
		fmt.Printf("\n")

		reportData.generateScenarioHeader(s, config)

		if s.Concurrent == true {
			fmt.Printf("\n")
			fmt.Printf("Current targets:\n %v\n", multipleTargets(s))
			fmt.Println()

			executeConcurrentTargets(env, config, s, reportData)
		} else if s.Timed == true {
			for _, t := range s.Targets {
				fmt.Printf("\n")
				fmt.Printf("Current timed target: %v\n", t.URL)

				processTimedTarget(env, t, s.Name, config.Thresholds, reportData)
			}
		} else {

			for _, t := range s.Targets {
				fmt.Printf("\n")
				fmt.Printf("Current target: %v\n", t.URL)

				processTarget(env, t, s.Name, config.Thresholds, reportData)
			}
		}

		timeout, err := time.ParseDuration(config.Timeout)
		if err != nil {
			fmt.Printf("Invalid timeout, scenarios will execute immidiately.")
		}

		time.Sleep(timeout)
	}

	fmt.Printf("*** Testing completed.\nTime elapsed: %v", time.Since(start))

	reportData.generateFooter(time.Since(start).String())

	return reportData
}
