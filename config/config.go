package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Target struct {
	URL               string `yaml:"url"`
	Workers           int    `yaml:"workers"`
	Requests          int    `yaml:"requests"`
	DurationMinutes   int    `yaml:"durationMinutes"`
	RequestsPerMinute int    `yaml:"requestsPerMinute"`
}

type Scenario struct {
	Name       string   `yaml:"name"`
	Concurrent bool     `yaml:"concurrent"`
	Timed      bool     `yaml:"timed"`
	Targets    []Target `yaml:"targets,flow"`
}

type Config struct {
	URL        string     `yaml:"url"`
	Scenarios  []Scenario `yaml:"scenarios,flow"`
	Thresholds []int64    `yaml:"thresholds"`
	Timeout    string     `yaml:"timeout"`
}

func (c *Config) readConfig(data []byte) error {
	if err := yaml.Unmarshal(data, c); err != nil {
		return err
	}
	return nil
}

func GetConfig(file string) Config {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		panic(err)
	}

	var config Config
	if err := config.readConfig(data); err != nil {
		panic(err)
	}
	return config
}
