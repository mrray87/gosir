FROM golang AS builder
RUN mkdir -p /go/src/gosir
RUN mkdir -p /go/src/gosir/reports
WORKDIR /go/src/gosir
ADD . .
RUN go get -d ./...
RUN go build -a -o gosir
ENTRYPOINT [ "./gosir" ]