package main

import (
	"testing"
)

func buildChartDataTest(t *testing.T) {
	var results []Result
	results = append(results, Result{ID: 1, Code: 200, Time: 255, Error: ""})
	results = append(results, Result{ID: 2, Code: 200, Time: 650, Error: ""})
	results = append(results, Result{ID: 3, Code: 200, Time: 650, Error: ""})

	x, y := buildChartData(results, 3)

	if len(x) > 3 {
		t.Error("Invalid length of chart data")
	}

	if len(y) > 3 {
		t.Error("Invalid length of chart data")
	}

	if x[1] != 2.0 {
		t.Error("Invalid X node value in chart data")
	}

	if y[1] != 650.0 {
		t.Error("Invalid Y node value in chart data")
	}
}
