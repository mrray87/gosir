package main

import (
	"testing"
)

func TestGetResultsBuckets(t *testing.T) {
	var thresholdLevels = []int64{300, 600, 900, 1200}
	var buckets []*ResultBucket
	buckets = createResultBuckets(thresholdLevels)
	if buckets[0].Low != 0 {
		t.Error("Ivalid low threshold value on bucket.")
	}
	if buckets[1].Low != 300 {
		t.Error("Ivalid low threshold value on bucket.")
	}
	if buckets[3].High != 1200 {
		t.Error("Ivalid high threshold value on bucket.")
	}
	if len(buckets) != 4 {
		t.Error("Invalid number of buckets!")
	}

}

func TestResultBucketFill(t *testing.T) {
	var results []Result
	results = append(results, Result{ID: 1, Code: 200, Time: 255, Error: ""})
	results = append(results, Result{ID: 2, Code: 200, Time: 650, Error: ""})

	var bucket ResultBucket
	bucket.Low = 500
	bucket.High = 800

	bucket.fill(results)

	if bucket.Timers[0] != 650 {
		t.Error("Soring failed.")
	}
}

func TestErrorBucketFill(t *testing.T) {
	var results []Result
	results = append(results, Result{ID: 1, Code: 200, Time: 255, Error: "error1"})
	results = append(results, Result{ID: 2, Code: 500, Time: 650, Error: "error2"})
	results = append(results, Result{ID: 3, Code: 403, Time: 650, Error: ""})

	var bucket ErrorBucket
	bucket.fill(results)

	if len(bucket.errors) != 2 {
		t.Error("Soring failed.")
	}
}

func TestResultProcessor(t *testing.T) {
	var thresholds = []int64{300, 600}
	var results []Result
	results = append(results, Result{ID: 1, Code: 200, Time: 255, Error: ""})
	results = append(results, Result{ID: 2, Code: 200, Time: 450, Error: ""})
	results = append(results, Result{ID: 3, Code: 200, Time: 650, Error: ""})

	buckets := processResults(results, thresholds)
	if len(buckets[0].Timers) != 1 {
		t.Error("Processing error: invalid threshold category.")
	}
	if buckets[1].Timers[0] != 450 {
		t.Error("Processing error: invalid threshold category.")
	}
}

func TestResultAverage(t *testing.T) {
	var bucket ResultBucket
	bucket.Timers = []int64{200, 300, 400}

	a := bucket.average()

	if a != 300 {
		t.Error("Invalid average.")
	}
}

func TestErrorCollection(t *testing.T) {
	var errorBucket ErrorBucket
	var errorCollection []ErrorGroup
	errorBucket.errors = append(errorBucket.errors, RequestError{1, "ErrorType 1", 10})
	errorBucket.errors = append(errorBucket.errors, RequestError{1, "ErrorType 2", 10})
	errorBucket.errors = append(errorBucket.errors, RequestError{1, "ErrorType 1", 10})
	errorBucket.errors = append(errorBucket.errors, RequestError{1, "ErrorType 2", 10})
	errorBucket.errors = append(errorBucket.errors, RequestError{1, "ErrorType 2", 10})

	errorCollection = sortErrors(&errorBucket)

	if len(errorCollection) != 2 {
		t.Error("Sorting error - wrong collection size")
	}

	if errorCollection[0].errorQuantity != 2 {
		t.Error("Sorting error - wrong error type assessment")
	}

	if errorCollection[0].errorType != "ErrorType 1" {
		t.Error("Sorting error - wrong error type assessment")
	}

	if errorCollection[1].errorQuantity != 3 {
		t.Error("Sorting error - wrong error type assessment")
	}

	if errorCollection[1].errorType != "ErrorType 2" {
		t.Error("Sorting error - wrong error type assessment")
	}
}
