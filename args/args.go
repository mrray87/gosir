package args

import (
	"errors"
	"strings"
)

// Arguments - test execution arguments structure
type Arguments struct {
	URL   string
	Suite string
}

// HeaderArgument - any header item that needs to be added to the request
type HeaderArgument struct {
	Name  string
	Value string
}

// GetArgs - fetches predefined arguments
func GetArgs(args []string) Arguments {
	var a Arguments

	for _, argument := range args {
		if strings.Contains(argument, "url=") {
			a.URL = argument[4:]
		}

		if strings.Contains(argument, "suite=") {
			a.Suite = argument[6:]
		}
	}

	// Default `url` value will come from the suite
	// Populate with default suite in no argument is provided
	if a.Suite == "" {
		a.Suite = "config/default.yml"
	}

	return a
}

// FindAuthToken - gets the CDN auth token
func FindAuthToken(args []string) (HeaderArgument, error) {
	var t HeaderArgument

	for _, argument := range args {
		if strings.Contains(argument, "x-access-token=") {
			t.Name = "x-access-token"
			t.Value = argument[15:]
			return t, nil
		}
	}
	return HeaderArgument{"", ""}, errors.New("No auth token supplied")
}
