package args

import (
	"fmt"
	"testing"
)

func TestGetArgs(t *testing.T) {
	args := []string{"url=url", "suite=suite", "token=token"}

	a := GetArgs(args)

	if a.URL != "url" {
		t.Error("Argument not found")
	}

	if a.Suite != "suite" {
		t.Error("Argument not found")
	}
}

func TestGetArgsReorder(t *testing.T) {
	args := []string{"suite=suite", "token=token", "url=url"}

	a := GetArgs(args)

	if a.URL != "url" {
		t.Error("Argument not found")
	}

	if a.Suite != "suite" {
		t.Error("Argument not found")
	}
}

func TestFindAuthToken(t *testing.T) {
	args := []string{"x-access-token=trueToken", "token=token", "url=url"}

	token, err := FindAuthToken(args)
	if err != nil {
		fmt.Println(err)
		t.Error("Token not found")
	}

	if token.Name != "x-access-token" {
		t.Error("Invalid token type")
	}

	if token.Value != "trueToken" {
		t.Error("Invalid token")
	}
}
