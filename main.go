package main

import (
	"os"
	"gosir/args"
)

func main() {
	a := args.GetArgs(os.Args)

	suiteName := a.Suite[7 : len(a.Suite)-4]

	results := processTargets(a.Suite, a.URL)

	generateResultsReport(results, "reports/"+suiteName+"_results.csv")
}
