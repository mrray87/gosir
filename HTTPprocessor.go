package main

import (
	"fmt"
	"time"
)

type ResultBucket struct {
	Low    int64
	High   int64
	Timers []int64
}

type RequestError struct {
	code    int
	err     string
	timeout int64
}

type ErrorBucket struct {
	errors []RequestError
}

type ErrorGroup struct {
	errorCode     int
	errorType     string
	errorQuantity int
}

func (b *ResultBucket) fill(results []Result) {
	for _, res := range results {
		if res.Code == 200 && res.Time > b.Low && res.Time < b.High {
			b.Timers = append(b.Timers, res.Time)
		}
	}
}

func (e *ErrorBucket) fill(results []Result) {
	for _, res := range results {
		if res.Code != 200 {
			e.errors = append(e.errors, RequestError{code: res.Code, err: res.Error, timeout: res.Time})
		}
	}
}

func sortErrors(e *ErrorBucket) []ErrorGroup {
	var errorCollections []ErrorGroup

	for _, res := range e.errors {
		if len(errorCollections) == 0 {
			errorCollections = append(errorCollections, ErrorGroup{res.code, res.err, 1})
			continue
		}

		errorExists := func() bool {
			for _, e := range errorCollections {
				if e.errorType == res.err {
					return true
				}
			}
			return false
		}

		if errorExists() == false {
			errorCollections = append(errorCollections, ErrorGroup{res.code, res.err, 1})
		} else {
			for i, e := range errorCollections {
				if e.errorType == res.err {
					errorCollections[i].errorQuantity++
					break
				}
			}
		}
	}

	return errorCollections
}

func (b *ResultBucket) average() int64 {
	if len(b.Timers) == 0 {
		return 0
	}

	var total int64
	for _, t := range b.Timers {
		total += t
	}
	return total / int64(len(b.Timers))
}

func createResultBuckets(thresholds []int64) []*ResultBucket {
	var buckets []*ResultBucket
	var previousThreshold int64
	for _, currentThreshold := range thresholds {
		b := &ResultBucket{Low: previousThreshold, High: currentThreshold, Timers: nil}
		buckets = append(buckets, b)
		previousThreshold = currentThreshold
	}
	return buckets
}

func processResults(results []Result, thresholds []int64) []*ResultBucket {
	buckets := createResultBuckets(thresholds)
	for _, b := range buckets {
		b.fill(results)
	}
	return buckets
}

func printResults(bucket []*ResultBucket) {
	for _, b := range bucket {
		d := time.Duration(b.average()) * time.Millisecond
		fmt.Printf("* %v requests timed between %v and %v. Average time: %v\n", len(b.Timers), b.Low, b.High, d)
	}
}

func processErrors(results []Result) ErrorBucket {
	var errorBucket ErrorBucket
	errorBucket.fill(results)
	return errorBucket
}

func printErrors(e ErrorBucket) {
	fmt.Printf("Errors: %v\n", len(e.errors))
}
