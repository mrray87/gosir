# Gosir Performance - Load testing tool written in Golang

## Description:
The app will create a specified number of workers, that will concurrently perform `GET` requests on predefined API endpoints and will record timoeouts and possible errors in form of logs and CSV reports. Web-service, endpoits, numbers of workers and requests are configurable and allow for different scenarios to be executed in any one test

## Requirements:
To build the project, `go version go1.12.3 darwin/amd64` is required. To install, navigate to https://golang.org/doc/install

## Usage:

To build, run:
```bash
go build
```

To build and run locally, run:
```bash
./run.sh
```

To execute a custom suite, after building run:
```bash
./gosir "url=config/custom_suite.yml"
```

To overwrite the env URL in the suite, after building run:
```bash
./gosir "suite=config/custom_suite.yml" "url=https://yourenv.com"
```

#### Note: The 3'rd argument is reserved for the access token

To build in docker, run:
```bash
docker build -t gosir .
```

To execute in docker, run:
```bash
  docker run --rm -it -v $PWD/reports:/go/src/gosir/reports gosir
# Here the `/tmp` folder is a Docker-shared dir in your system, used to store reports
```

## Configuration:

To configure, create or edit `config/config.yml`:

```yaml
url: https://your-server.com            # server URL

scenarios:                              # test scenarios
  - name: Lorem ipsum dolor sit amet    # scenario name
    concurrent: true                    # run targets concurently
    targets:                            # targets
      - url: /users/                    # target URL
        workers: 50                     # workers
        requests: 500                   # requests total
      - url: /users/photos
        workers: 50
        requests: 500

scenarios:                              # test scenarios
  - name: Lorem ipsum dolor sit amet    # scenario name
    timed: true                         # run target continuously in time
    targets:                            # targets
      - url: /users/                    # target URL
        durationMinutes: 50             # minutes to run the test
        requestsPerMinute: 500          # number of requests done per minute

thresholds:                             # result split steps
  - 500                                 # first group is
  - 1000                                # 0 to 500 ms
  - 20000                               # in this example
```