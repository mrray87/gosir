package main

import (
	"encoding/csv"
	"os"
	"strconv"
	"gosir/config"
	"time"
)

type reportData [][]string

func (r *reportData) generateReportHeaderData(env string, config config.Config, thresholds []int64) {
	*r = append(*r, []string{"LOAD TESTING RESULTS", env, time.Now().UTC().Format("2006-01-02T15:04")})
}

func (r *reportData) generateScenarioHeader(s config.Scenario, cfg config.Config) {
	scenarioHeader := []string{}
	if s.Concurrent == true {
		scenarioHeader = append(scenarioHeader, s.Name+", Concurrent", "Target endpoint", "Total requests")
	} else if s.Timed == true {
		scenarioHeader = append(scenarioHeader, s.Name+", Timed", "Target endpoint", "Total requests")
	} else {
		scenarioHeader = append(scenarioHeader, s.Name, "Target endpoint", "Requests")
	}

	var lastThreshold int64
	for _, t := range cfg.Thresholds {
		scenarioHeader = append(scenarioHeader, strconv.FormatInt(lastThreshold, 10)+"-"+strconv.FormatInt(t, 10))
		lastThreshold = t
	}

	scenarioHeader = append(scenarioHeader, "Target time", "Errors")

	*r = append(*r, scenarioHeader)
}

func (r *reportData) generateTargetLine(target string, requests string, results []*ResultBucket, time string, errorGroups []ErrorGroup) {
	targetLine := []string{"", target, requests}

	for _, r := range results {
		quantity := strconv.Itoa(len(r.Timers))
		targetLine = append(targetLine, quantity)
	}

	targetLine = append(targetLine, time)

	for _, e := range errorGroups {
		targetLine = append(targetLine, strconv.Itoa(e.errorCode)+"/"+strconv.Itoa(e.errorQuantity))
	}

	*r = append(*r, targetLine)
}

func (r *reportData) generateFooter(t string) {
	*r = append(*r, []string{"Testing completed in:", t})
}

func generateResultsReport(data *reportData, filename string) {
	file, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range *data {
		_ = writer.Write(value)
	}
}
