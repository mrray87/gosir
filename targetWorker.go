package main

import (
	"fmt"
	"strconv"
	"gosir/config"
	"time"
)

func processTarget(URL string, target config.Target, scenario string, thresholds []int64, reportData *reportData) {
	start := time.Now()

	savedResults := executeJobs(URL, target.URL, target.Workers, target.Requests)

	r := processResults(savedResults, thresholds)
	e := processErrors(savedResults)

	drawChart(savedResults, target.URL, scenario)

	printResults(r)
	printErrors(e)
	fmt.Println()

	resTime := time.Since(start)
	t := strconv.FormatInt(int64(resTime/time.Millisecond), 10)

	errorGroups := sortErrors(&e)
	reportData.generateTargetLine(target.URL, strconv.Itoa(target.Requests), r, t, errorGroups)
}

func processTimedTarget(URL string, target config.Target, scenario string, thresholds []int64, reportData *reportData) {
	start := time.Now()

	savedResults := executeTimedJobs(URL, target.URL, target.DurationMinutes, target.RequestsPerMinute)

	r := processResults(savedResults, thresholds)
	e := processErrors(savedResults)

	drawChart(savedResults, target.URL, scenario)

	printResults(r)
	printErrors(e)
	fmt.Println()

	resTime := time.Since(start)
	t := strconv.FormatInt(int64(resTime/time.Millisecond), 10)

	errorGroups := sortErrors(&e)
	reportData.generateTargetLine(target.URL, strconv.Itoa(target.DurationMinutes*target.RequestsPerMinute), r, t, errorGroups)
}

func targetsWorker(URL string, target config.Target, scenario string, thresholds []int64, jobs <-chan int, results chan<- int, reportData *reportData) {
	for j := range jobs {
		processTarget(URL, target, scenario, thresholds, reportData)
		results <- j
	}
}
