package main

import (
	"context"
	"net/http"
	"os"
	"gosir/args"
	"time"
)

type Result struct {
	ID    int
	Code  int
	Time  int64
	Error string
}

func addHeaders(r *http.Request) {
	// Adding auth token header
	token, err := args.FindAuthToken(os.Args)
	if err == nil {
		r.Header.Set(token.Name, token.Value)
	}
}

func workerGet(server string, url string, jobs <-chan int, results chan<- Result) {
	for j := range jobs {

		start := time.Now()

		client := &http.Client{}
		request, err := http.NewRequest("GET", server+url, nil)
		if err != nil {
			panic(err)
		}

		addHeaders(request)

		res, err := client.Do(request)
		if err != nil {
			result := Result{ID: j, Code: 0, Time: 0, Error: err.Error()}
			results <- result
			continue
		}
		res.Body.Close()

		resTime := time.Since(start)
		t := int64(resTime / time.Millisecond)

		result := Result{ID: j, Code: res.StatusCode, Time: t, Error: ""}
		results <- result
	}
}

func workerTimedGet(ctx context.Context, server string, url string, results chan<- Result) {
	start := time.Now()

	client := &http.Client{}
	request, err := http.NewRequest("GET", server+url, nil)
	if err != nil {
		panic(err)
	}

	addHeaders(request)

	res, err := client.Do(request)

	if err != nil {
		result := Result{ID: 0, Code: 0, Time: 0, Error: err.Error()}
		results <- result
		return
	}

	res.Body.Close()

	resTime := time.Since(start)
	t := int64(resTime / time.Millisecond)

	result := Result{ID: 0, Code: res.StatusCode, Time: t, Error: ""}
	results <- result

	select {
	case <-ctx.Done():
		result := Result{ID: 0, Code: 1, Time: 0, Error: "Context timeout - request ignored."}
		results <- result
		return
	}
}
