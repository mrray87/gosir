package main

import (
	"gosir/config"
)

func executeConcurrentTargets(env string, cfg config.Config, s config.Scenario, reportData *reportData) {
	jobs := make(chan int, len(s.Targets))
	results := make(chan int, len(s.Targets))

	for w := 0; w < len(s.Targets); w++ {
		go targetsWorker(env, s.Targets[w], s.Name, cfg.Thresholds, jobs, results, reportData)
	}

	for r := 0; r < len(s.Targets); r++ {
		jobs <- r
	}
	close(jobs)

	for a := 0; a < len(s.Targets); a++ {
		<-results
	}
}
